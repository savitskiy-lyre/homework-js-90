const express = require('express');
const cors = require('cors');
const {nanoid} = require("nanoid");
const server = express();
const expressWs = require('express-ws')(server);

const port = 8000;
server.use(cors());
server.use(express.json());

const activeCanvasConnections = {};
let canvasArrData = [];

server.ws('/canvas', (ws, req) => {
    const id = nanoid();
    console.log('client connected! id=' + id);
    activeCanvasConnections[id] = ws;
    if (canvasArrData.length > 0) {
        const newMsg = JSON.stringify({type: 'CANVAS_NEW_IMG', canvas: canvasArrData});
        ws.send(newMsg);
    }

    ws.on('message', msg => {
        const parsedMsg = JSON.parse(msg);
        switch (parsedMsg.type) {
            case 'CANVAS_NEW_IMG':
                Object.keys(activeCanvasConnections).forEach(key => {
                    canvasArrData = canvasArrData.concat(parsedMsg.canvas);
                    if (key === id) return;
                    const connection = activeCanvasConnections[key];
                    const newMsg = JSON.stringify({...parsedMsg, canvas: parsedMsg.canvas});
                    connection.send(newMsg);
                })
                break;
        }
    })

    ws.on('close', (msg) => {
        console.log('client disconnected ! id=' + id);
        delete activeCanvasConnections[id];
        if (Object.keys(activeCanvasConnections).length === 0) {
            canvasArrData = [];
        }
    });
});

server.listen(port, () => {

    console.log(`Server started on ${port} port!`);

});
