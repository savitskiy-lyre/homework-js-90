import React, {useEffect, useRef, useState} from 'react';

const Canvas = () => {
    const canvasRef = useRef(null);
    const ws = useRef(null);
    const [canvas, setCanvas] = useState({
        mouseDown: false,
        pixelsArray: []
    });
    const [selectSt, setSelectSt] = useState('black');

    const canvasMouseMoveHandler = event => {
        if (canvas.mouseDown) {
            //event.persist();
            const clientX = event.clientX;
            const clientY = event.clientY;
            setCanvas(prevState => {
                return {
                    ...prevState,
                    pixelsArray: [...prevState.pixelsArray, {
                        x: clientX,
                        y: clientY,
                        color: selectSt,
                    }]
                };
            });
            const context = canvasRef.current.getContext('2d');
            context.beginPath();
            context.fillStyle = selectSt;
            context.arc(event.clientX - 9, event.clientY - 60, 3, 0, Math.PI * 2, false)
            context.fill();
        }
    };

    const mouseDownHandler = event => {
        setCanvas({...canvas, mouseDown: true});
    };

    const mouseUpHandler = event => {
        ws.current.send(JSON.stringify({canvas: canvas.pixelsArray, type: "CANVAS_NEW_IMG"}));
        setCanvas({...canvas, mouseDown: false, pixelsArray: []});
    };

    useEffect(() => {
        ws.current = new WebSocket('ws://localhost:8000/canvas');

                        ws.current.onmessage = event => {
                                        const parsedMsg = JSON.parse(event.data);

                                        if (parsedMsg.type === 'CANVAS_NEW_IMG') {
                                            const context = canvasRef.current.getContext('2d');
                                            parsedMsg.canvas.forEach(coordinate => {
                                                context.beginPath();
                                                context.fillStyle = coordinate.color;
                                                context.arc(coordinate.x - 9, coordinate.y - 60, 3, 0, Math.PI * 2, false)
                                                context.fill();
                                            })
                                            }
                        }
    }, []);

    return (
        <div>
            <div style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                textAlign: 'center',
                margin: '10px 10px',
            }}>
                <select
                    value={selectSt}
                    onChange={(e) => setSelectSt(e.target.value)}
                    style={{
                        fontSize: '20px',
                        padding: '8px',
                        margin: '0 10px',
                    }}
                >
                    <option value="black">black</option>
                    <option value="red">red</option>
                    <option value="green">green</option>
                    <option value="yellow">yellow</option>
                    <option value="grey">grey</option>
                </select>
                <div style={{width: '40px', height: '40px', background: selectSt}}/>
            </div>
            <canvas
                ref={canvasRef}
                style={{
                    border: '1px solid black',
                }}
                width={1500}
                height={1000}
                onMouseDown={mouseDownHandler}
                onMouseUp={mouseUpHandler}
                onMouseMove={canvasMouseMoveHandler}
            />
        </div>
    );
};
export default Canvas;